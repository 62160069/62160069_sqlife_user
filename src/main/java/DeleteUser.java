
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author BmWz
 */
public class DeleteUser {

    public static void main(String[] args) {
        Connection conn = null;
        Statement stmt = null;

        try {
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite:user.db");
            conn.setAutoCommit(false);
            stmt = conn.createStatement();

            stmt.executeUpdate("DELETE  from USER where id=3");
            conn.commit();

            ResultSet rs = stmt.executeQuery("SELECT * FROM USER");
            while (rs.next()) {
                int id = rs.getInt("id");
                String username = rs.getString("username");
                String password = rs.getString("password");

                System.out.println("id = " + id);
                System.out.println("username = " + username);
                System.out.println("password = " + password);
                System.out.println();
            }
            rs.close();
            stmt.close();
            conn.close();

        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DeleteUser.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(DeleteUser.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
