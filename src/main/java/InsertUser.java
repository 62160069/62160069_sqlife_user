
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author BmWz
 */
public class InsertUser {
    public static void main(String[] args) {
        Connection conn = null;
        Statement stmt = null;

        try {
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite:user.db");
         conn.setAutoCommit(false);
            stmt = conn.createStatement();
            String sql = "INSERT INTO USER (USERNAME,PASSWORD) "
                    + "VALUES ('Admin','password');";
            stmt.executeUpdate(sql);
            sql = "INSERT INTO USER (USERNAME,PASSWORD) "
                    + "VALUES ('Bm','password');";
            stmt.executeUpdate(sql);
            sql = "INSERT INTO USER (USERNAME,PASSWORD) "
                    + "VALUES ('Fon','password');";
            stmt.executeUpdate(sql);
            sql = "INSERT INTO USER (USERNAME,PASSWORD) "
                    + "VALUES ('Re','password');";
            stmt.executeUpdate(sql);
            sql = "INSERT INTO USER (USERNAME,PASSWORD) "
                    + "VALUES ('Van','password');";
            stmt.executeUpdate(sql);
            sql = "INSERT INTO USER (USERNAME,PASSWORD) "
                    + "VALUES ('New','password');";
            stmt.executeUpdate(sql);
            
            conn.commit();
            stmt.close();
            conn.close();
            
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(InsertUser.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(InsertUser.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
