
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author BmWz
 */
public class ConnectDatabaseUser {

    public static void main(String[] args) {
        Connection conn = null;
        String dbname = "user.db";
        try {
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite:" + dbname);
        } catch (ClassNotFoundException ex) {
            System.out.println("Library org.sqlite.JDBC not found!!!");
            System.exit(0);
        } catch (SQLException ex) {
            System.out.println("Unabel to open database!!!");
            System.exit(0);
        }
        System.out.println("Opened database succesfuly");
    }
}
